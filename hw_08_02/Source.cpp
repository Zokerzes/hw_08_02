﻿#include <iostream>
#include "time.h"
using namespace std;
void task1()
{
	/*«јƒјЌ»≈ 1
¬ одномерном массиве, заполненном случайными числами,
определить минимальный и максимальный элементы.*/
	setlocale(LC_ALL, "rus");
	int mas[50];
	int min = 100, max = 0;
	for (int i = 0; i < 50; i++)
	{
		mas[i] = 1 + rand() % 99;
	}
	cout << "массив\n";
	for (int i = 0; i < 50; i++)
	{
		cout << mas[i] << "  ";
		if (max < mas[i]) max = mas[i];
		if (min > mas[i]) min = mas[i];
	}
	cout << "\nmin = " << min << "   max = " << max;
}

void task2()
{
	/*«јƒјЌ»≈ 2
¬ одномерном массиве, заполненном случайными числами
в заданном пользователем диапазоне, найти сумму элементов,
значени¤ которых меньше указанного пользователем.*/
	setlocale(LC_ALL, "rus");
	int level, sum = 0, a, b;
	const int N = 20;
	int mas[N];
	srand(time(0));

	cout << "введите значение уровн¤, начальное и конечное значение диапазона";
	cin >> level >> a >> b;
	for (int i = 0; i < N; i++)
	{
		mas[i] = a + rand() % (b - a);
		cout << mas[i] << " ";
	}
	for (int i = 0; i < N; i++)
	{
		if (mas[i] < level)
		{
			sum = sum + mas[i];
		}
	}
	cout << "\nSum = " << sum;


}

void task3()
{
	/*ЗАДАНИЕ 3
Пользователь вводит прибыль фирмы за год (12 месяцев).
Затем пользователь вводит диапазон (например, 3 и 6 — поиск
между 3-м и 6-м месяцем). Необходимо определить месяц,
в котором прибыль была максимальна и месяц, в котором
прибыль была минимальна с учетом выбранного диапазона.*/

	setlocale(LC_ALL, "rus");
	int debet[12]{ 50000,60000,40000,30000,50000,40500,84512,32542,12546,52145,57842,36547 };	//чтобы не вводить каждый раз
	int ds, de, max, min, mi = 0, ma = 0;
	cout << "введите начальный и конечный номер месяца (1-12):\n";
	cin >> ds >> de;

	//for (int i = 0; i < 12; i++)																// раскоментировать для ручного ввода данных
	//{
	//	  cout << "введите прибыль в " << i+1 << "месяце";
	//	  cin >> debet[i];
	//}

	
	
	max = debet[0];
	min = debet[11];
	for (int i = ds - 1; i <= de - 1; i++)
	{
		if (max < debet[i])
		{
			max = debet[i];
			ma = i;
		};
		if (min > debet[i])
		{
			min = debet[i];
			mi = i;
		};
	}
	cout << "прибыль минимальна (" << min << ") в " << mi << " месяце \nприбыль максимальна (" << max << ") в " << ma << " месяце ";
}
void task4()
{
	/*ЗАДАНИЕ 4
В одномерном массиве, состоящем из N вещественных
чисел вычислить:
■■ Сумму отрицательных элементов.
■■ Произведение элементов, находящихся между min и max элементами.
■■ Произведение элементов с четными номерами.
■■ Сумму элементов, находящихся между первым и последним отрицательными элементами.*/
	setlocale(LC_ALL, "rus");
	int level, sum_neg = 0, sum_neg1 = 0, a = -100, b = 100, mi = 0, ma = 0, mi_neg = 0, ma_neg = 0, max, min;
	bool f1 = false, f2 = false;
	long int prod_min_max = 1, prod_chet = 1;
	const int N = 20;
	int mas[N];
	srand(time(0));
	for (int i = 0; i < N; i++)							//заполняем массив случайными числами в диапозоне от A до B
	{
		mas[i] = a + rand() % (b-a);
	}
	cout << "\nмассив\n";
	for (int i = 0; i < N; i++)							//вывод элементов массива для контроля
	{
		cout << mas[i] << "  ";
	}
	for (int i = 0; i < N; i++)							//сумма отрицательных элементов
	{
		if (mas[i] < 0)
		{
			sum_neg += mas[i];
		}
	}
	max = mas[0];										//сброс значений на начальные
	min = mas[N - 1];
	mi = 0;
	ma = 0;
	prod_min_max = 1;
	for (int i = 0; i < N; i++)							//находим индексы min и max элемента.
	{
		if (max < mas[i])
		{
			max = mas[i];
			ma = i;
		};
		if (min > mas[i])
		{
			min = mas[i];
			mi = i;
		};
	}

	if (mi <= ma)										//Произведение элементов, находящихся между min и max элементами.
	{
		for (int i = mi+1; i < ma; i++)
		{
			prod_min_max *= mas[i];
		}
	}
	else
	{
		for (int i = ma+1; i < mi; i++)
		{
			prod_min_max *= mas[i];
		}
	};
	prod_chet = 1;
	for (int i = 0; i < N; i++)							//Произведение элементов с четными номерами.
	{
		if (!(mas[i] % 2))
		{
			prod_chet *= mas[i];
		};
	}
														//Сумму элементов, находящихся между первым и последним отрицательными элементами
	
	for (int i = 0; i < N; i++)
	{
		if (mas[i] < 0 /* && f1 == false*/)
		{
			mi_neg = i;									//первый отрицательный элемент
			//f1 = true;
			break;
		};
	}
	for (int i = N-1; i >= 0; i--)
	{
		if (mas[i] < 0/* && f2 == false*/)
		{
			ma_neg = i;									//последний (первый с конца) отрицательный элемент
			//f2 = true;
			break;
		};
	}
	sum_neg1 = 0;
	for (int i = mi_neg+1; i < ma_neg ; i++)
	{
		sum_neg1 += mas[i];
	}
	//выводим полученые данные в консоль

	cout << "\n\nсумма отрицательных элементов -> " << sum_neg << "\n\nПроизведение элементов, находящихся между min и max элементами -> " << prod_min_max;
	cout << "\n\nПроизведение элементов с четными номерами. -> " << prod_chet;
	cout << "\n\nСумма элементов, находящихся между первым и последним отрицательными элементами -> " << sum_neg1;
}


int main()
{
	int t;
	do
	{
		cout << "\n\nenter # task(to exit enter negative value):  ";
		cin >> t;
		switch (t)
		{
		case 1:
			task1();
			break;
		case 2:
			task2();
			break;
		case 3:
			task3();
			break;
		case 4:
			task4();
			break;
		default:
			break;
		}
	} while (t >= 0);

	return 0;
}